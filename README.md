# Magento 2.3.1

Copyright by UET-University of Engineering and Technology

## Pull code here

https://gitlab.com/magento-uet-22/docker.git

## Follow this step to run this project

```bash
# 1. Install docker

# 2. Execute docker compose
docker-compose up

# 3. copy database to mysql
docker cp ./Ecomerce/database.sql (docker_compose_db_1):/var/lib/mysql

# 4. import database.sql to database magentodemo
mysql -uroot -p magentodemo < database.sql
#Enter password: root

# 5. restart (docker_compose_magento_1)

# 6. access localhost:8000
# 7. access localhost:8000/admin
```
